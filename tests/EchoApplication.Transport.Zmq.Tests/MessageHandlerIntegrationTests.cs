﻿using System;
using System.Threading;
using NUnit.Framework;

namespace EchoApplication.Transport.Zmq.Tests
{
    [TestFixture]
    public class MessageHandlerIntegrationTests
    {
        [Test]
        public void WhenClientSendMessageThenServerShouldReceiveIt()
        {
            var server = new ZmqServerMessageHandler(3000, new ProtobufSerializer());
            Thread.Sleep(200);
            var client = new ZmqClientMessageHandler("tcp://127.0.0.1:3000", new ProtobufSerializer());

            AutoResetEvent sync = new AutoResetEvent(false);

            var clientId = 1;

            server.SubscribeBy<TestDto>(1, dto => sync.Set());
            
            client.Send(new TestDto {ClientId = clientId, Str = "msg"});

            if (sync.WaitOne(TimeSpan.FromSeconds(5)))
            {
                client.Dispose();
                server.Dispose();
                Assert.Pass();
                
            }
            else
            {
                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }
        }

        [Test]
        public void WhenServerSendMessageToRegiseredClientThenClientShouldReceiveIt()
        {
            var server = new ZmqServerMessageHandler(3001, new ProtobufSerializer());
            Thread.Sleep(200);
            var client = new ZmqClientMessageHandler("tcp://127.0.0.1:3001", new ProtobufSerializer());

            AutoResetEvent sync = new AutoResetEvent(false);

            var clientId = 1;

            client.Subscribe<TestDto>(dto => sync.Set());

            //Register client
            client.Send(new TestDto { ClientId = clientId, Str = "msg" });

            Thread.Sleep(500);

            server.Send(new TestDto { ClientId = clientId, Str = "server" });

            if (sync.WaitOne(TimeSpan.FromSeconds(5)))
            {
                client.Dispose();
                server.Dispose();
                Assert.Pass();
            }
            else
            {
                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }
        }

        [Test]
        public void WhenClientSendSeveralMessageThenServerShouldReceiveItAll()
        {
            var server = new ZmqServerMessageHandler(3002, new ProtobufSerializer());
            Thread.Sleep(200);
            var client = new ZmqClientMessageHandler("tcp://127.0.0.1:3002", new ProtobufSerializer());

            AutoResetEvent sync = new AutoResetEvent(false);

            var clientId = 1;

            server.SubscribeBy<TestDto>(1, dto => sync.Set());


            sync.Reset();
            client.Send(new TestDto { ClientId = clientId, Str = "msg" });
            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {

                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }

            sync.Reset();
            client.Send(new TestDto { ClientId = clientId, Str = "msg2" });
            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {

                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }

            sync.Reset();
            client.Send(new TestDto { ClientId = clientId, Str = "ms3" });
            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {

                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }

            client.Dispose();
            server.Dispose();
            Assert.Pass();
        }

        [Test]
        public void WhenServerSendSeveralMessageThenClientShouldReceiveItAll()
        {
            var server = new ZmqServerMessageHandler(3003, new ProtobufSerializer());
            Thread.Sleep(200);
            var client = new ZmqClientMessageHandler("tcp://127.0.0.1:3003", new ProtobufSerializer());

            AutoResetEvent sync = new AutoResetEvent(false);

            var clientId = 1;

            client.Subscribe<TestDto>(dto => sync.Set());

            //Register client
            client.Send(new TestDto { ClientId = clientId, Str = "msg" });

            Thread.Sleep(200);

            sync.Reset();
            server.Send(new TestDto { ClientId = clientId, Str = "server" });

            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {
                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }

            sync.Reset();
            server.Send(new TestDto { ClientId = clientId, Str = "server 1" });

            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {
                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }

            sync.Reset();
            server.Send(new TestDto { ClientId = clientId, Str = "server 2" });

            if (!sync.WaitOne(TimeSpan.FromSeconds(5)))
            {
                client.Dispose();
                server.Dispose();
                Assert.Fail();
            }


            client.Dispose();
            server.Dispose();
            Assert.Pass();
        }
    }
}
