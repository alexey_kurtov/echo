﻿using System;
using EchoApplication.Transport.DTO;
using EchoApplication.Transport.Zmq.Subscribers;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Transport.Zmq.Tests
{
    [TestFixture]
    public class CheckTypeSubscriberTests
    {
        [Test]
        public void WhenInitializeWithNullActionThenShouldThrowArgumentNullException()
        {
            Assert.Catch<ArgumentNullException>(() => new CheckTypeSubscriber<FakeDto1>(null, new Mock<ISerializer>().Object));
        }

        [Test]
        public void WhenInitializeWithNullSerializerThenShouldThrowArgumentNullException()
        {
            Assert.Catch<ArgumentNullException>(() => new CheckTypeSubscriber<FakeDto1>(dto1 => {  }, null));
        }

        [Test]
        public void WhenNotifiyWithCorrectTypeThenActionShouldExecuted()
        {

            var subscriber = new CheckTypeSubscriber<FakeDto1>(dto => { Assert.Pass(); }, new Mock<ISerializer>().Object);

            subscriber.Notify(1, new TransportMsg { Type = typeof(FakeDto1).Name });

            Assert.Fail();
        }

        [Test]
        public void WhenNotifiyWithIncorrectTypeThenActionShouldNotExecuted()
        {

            var subscriber = new CheckTypeSubscriber<FakeDto1>(dto => { Assert.Fail(); }, new Mock<ISerializer>().Object);

            subscriber.Notify(1, new TransportMsg { Type = typeof(FakeDto2).Name });

            Assert.Pass();
        }


        [Test]
        public void WhenUsubscribeThenSubscribeStatusShouldChange()
        {

            var subscriber = new CheckTypeSubscriber<FakeDto1>(dto => { Assert.Fail(); }, new Mock<ISerializer>().Object);

            Assert.That(subscriber.Unsubscribed, Is.False);

            subscriber.Unsubscribe();

            Assert.That(subscriber.Unsubscribed, Is.True);

        }
    }


    internal class FakeDto1 : ICommunicationMessage
    {
        public int ClientId { get; set; }
    }
    internal class FakeDto2 : ICommunicationMessage
    {
        public int ClientId { get; set; }
    }
}