﻿using System.IO;
using System.Runtime.Serialization;
using EchoApplication.Transport.DTO;
using NUnit.Framework;

namespace EchoApplication.Transport.Zmq.Tests
{
    [TestFixture]
    public class ProtobufSerializerTests
    {
        [Test]
        public void WhenSerializeNullMessageThenShouldThorwException()
        {
            var serializer = new ProtobufSerializer();
            Assert.Catch<SerializationException>(() => serializer.Serialize<TestDto>(null));
        }

        [Test]
        public void WhenDeserializeNullStreamThenShouldThorwException()
        {
            var serializer = new ProtobufSerializer();
            Assert.Catch<SerializationException>(() => serializer.Deserialize<TestDto>((Stream)null));
        }
        [Test]
        public void WhenDeserializeNullByteArrayThenShouldThorwException()
        {
            var serializer = new ProtobufSerializer();
            Assert.Catch<SerializationException>(() => serializer.Deserialize<TestDto>((byte[])null));
        }

        [Test]
        public void WhenSerializeObjectThenDeserializedObjectShouldEqualentWithIt()
        {
            var serializer = new ProtobufSerializer();

            var serializeDto = new TestDto() {Int = 432, Str = "test str", ClientId = 22};

            var data = serializer.Serialize(serializeDto);

            var deserializeDto = serializer.Deserialize<TestDto>(new MemoryStream(data));

            Assert.That(serializeDto.Str, Is.EqualTo(deserializeDto.Str));
            Assert.That(serializeDto.Int, Is.EqualTo(deserializeDto.Int));
            Assert.That(serializeDto.ClientId, Is.EqualTo(deserializeDto.ClientId));
        }
    }

    [DataContract]
    internal class TestDto : ICommunicationMessage
    {
        [DataMember(Order = 1)]
        public   string Str { get; set; }

        [DataMember(Order = 2)]
        public   int Int { get; set; }

        [DataMember(Order = 3)]
        public int ClientId { get; set; }
    }
}