﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using EchoApplication.Common;

namespace EchoApplication.Common.Tests
{
    [TestFixture]
    public class GuardTest
    {
        [Test]
        
        public void WhenParamNullThenShouldThrowArgumentNullException()
        {
            Assert.Catch<ArgumentNullException>(() => ((object) null).ThrowIfNull("param"));
        }

        [Test]
        public void WhenParamIsNotNullThenShouldNotThrowException()
        {
            Assert.DoesNotThrow(() => ("not null").ThrowIfNull("param"));
        }
    }
}
