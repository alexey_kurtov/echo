﻿using System;
using System.IO;
using System.Threading;
using EchoApplication.Domain;
using EchoApplication.Domain.Impl;
using EchoApplication.Transport;
using EchoApplication.Transport.DTO;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class MessageRouterTests
    {
        [Test]
        public void WhenInitializeWithNullMessageHandlerThenShouldThrowArgumentNullException()
        {
            var ex = Assert.Catch<ArgumentNullException>(() => new MessageRouter(null, new Mock<IRoomsController>().Object, new Mock<IClientsController>().Object));
            Assert.That(ex.ParamName, Is.EqualTo("messageHandler"));
        }

        [Test]
        public void WhenInitializeWithNullRoomsControllerThenShouldThrowArgumentNullException()
        {
            var ex = Assert.Catch<ArgumentNullException>(() => new MessageRouter(new Mock<IServerMessageHandler>().Object, null, new Mock<IClientsController>().Object));
            Assert.That(ex.ParamName, Is.EqualTo("roomsController"));
        }

        [Test]
        public void WhenInitializeWithNullClientsControllerThenShouldThrowArgumentNullException()
        {
            var ex = Assert.Catch<ArgumentNullException>(() => new MessageRouter(new Mock<IServerMessageHandler>().Object, new Mock<IRoomsController>().Object, null));
            Assert.That(ex.ParamName, Is.EqualTo("clientsController"));
        }

        [Test]
        public void WhenRouterInitializedThenShouldSubscribeToJoinRoomMessages()
        {
            var mock = new Mock<IServerMessageHandler>();
            var router = new MessageRouter(mock.Object, new Mock<IRoomsController>().Object, new Mock<IClientsController>().Object);
            
            mock.Verify(h => h.SubscribeAll<JoinMessage>(It.IsAny<Action<JoinMessage>>()), Times.Once);
        }

        [Test]
        public void WhenRouterDisposedThenShouldSubscribeToJoinRoomMessages()
        {
            var handlerMock = new Mock<IServerMessageHandler>();
            var subscriberMock = new Mock<ISubscriber>();

            handlerMock.Setup(h => h.SubscribeAll(It.IsAny<Action<JoinMessage>>())).Returns(subscriberMock.Object);

            var router = new MessageRouter(handlerMock.Object, new Mock<IRoomsController>().Object, new Mock<IClientsController>().Object);

            subscriberMock.Verify(h => h.Unsubscribe(), Times.Never);

            router.Dispose();

            subscriberMock.Verify(h => h.Unsubscribe(), Times.Once);
        }

        [Test]
        public void WhenJoinRoomMessageRisedThenClientShouldGetOrAddClientFromController()
        {
            var syncEvent = new AutoResetEvent(false);

            var handlerMock = new Mock<IServerMessageHandler>();
            var clientControllerMock = new Mock<IClientsController>();
            clientControllerMock.Setup(c => c.GetOrAdd(It.IsAny<int>())).Returns(new Mock<IClient>().Object);

            Action<JoinMessage> raiseMessage = null;
            handlerMock.Setup(h => h.SubscribeAll(It.IsAny<Action<JoinMessage>>())).Callback((Action<JoinMessage> a) =>
                                                                                                    {
                                                                                                        raiseMessage = a;
                                                                                                        syncEvent.Set();
                                                                                                    });

            var router = new MessageRouter(handlerMock.Object, new Mock<IRoomsController>().Object, clientControllerMock.Object);

            var joinMessage = new JoinMessage {ClientId = 1, RoomId = "TestRoom"};

            syncEvent.WaitOne(TimeSpan.FromSeconds(1));
            raiseMessage(joinMessage);

            clientControllerMock.Verify(h => h.GetOrAdd(It.Is<int>(id => id == joinMessage.ClientId)), Times.Once);
        }

        [Test]
        public void WhenJoinRoomMessageRisedThenClientShouldGetOrAddRoomFromController()
        {
            var syncEvent = new AutoResetEvent(false);

            var handlerMock = new Mock<IServerMessageHandler>();
            var clientControllerMock = new Mock<IClientsController>();
            clientControllerMock.Setup(c => c.GetOrAdd(It.IsAny<int>())).Returns(new Mock<IClient>().Object);

            var roomControllerMock = new Mock<IRoomsController>();

            Action<JoinMessage> raiseMessage = null;
            handlerMock.Setup(h => h.SubscribeAll(It.IsAny<Action<JoinMessage>>())).Callback((Action<JoinMessage> a) =>
            {
                raiseMessage = a;
                syncEvent.Set();
            });

            var router = new MessageRouter(handlerMock.Object, roomControllerMock.Object, clientControllerMock.Object);

            var joinMessage = new JoinMessage { ClientId = 1, RoomId = "TestRoom" };

            syncEvent.WaitOne(TimeSpan.FromSeconds(1));
            raiseMessage(joinMessage);

            roomControllerMock.Verify(h => h.GetOrAdd(It.Is<string>(id => id == joinMessage.RoomId)), Times.Once);
        }

        [Test]
        public void WhenJoinRoomMessageRisedThenClientShouldJoinToRoom()
        {
            var syncEvent = new AutoResetEvent(false);

            var handlerMock = new Mock<IServerMessageHandler>();
            var clientControllerMock = new Mock<IClientsController>();
            var clientMock = new Mock<IClient>();
            var roomControllerMock = new Mock<IRoomsController>();

            clientControllerMock.Setup(c => c.GetOrAdd(It.IsAny<int>())).Returns(clientMock.Object);

            var roomMock = new Mock<IRoom>();
            roomControllerMock.Setup(c => c.GetOrAdd(It.IsAny<string>())).Returns(roomMock.Object);

            Action<JoinMessage> raiseMessage = null;
            handlerMock.Setup(h => h.SubscribeAll(It.IsAny<Action<JoinMessage>>())).Callback((Action<JoinMessage> a) =>
            {
                raiseMessage = a;
                syncEvent.Set();
            });

            var router = new MessageRouter(handlerMock.Object, roomControllerMock.Object, clientControllerMock.Object);

            var joinMessage = new JoinMessage { ClientId = 1, RoomId = "TestRoom" };

            syncEvent.WaitOne(TimeSpan.FromSeconds(1));
            raiseMessage(joinMessage);

            clientMock.Verify(h => h.JoinToRoom(It.Is<IRoom>(r => r == roomMock.Object)), Times.Once);
        }
    }
}