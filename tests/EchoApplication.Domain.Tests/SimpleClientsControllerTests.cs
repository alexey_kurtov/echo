﻿using EchoApplication.Domain;
using EchoApplication.Domain.Impl;
using EchoApplication.Transport;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class SimpleClientsControllerTests
    {
        [Test]
        public void WhenTryGetNewClientThenNewClientInstanceShouldReturned()
        {
            var mock = new Mock<IClientFacrory>();

            var conrtoller = new SimpleClientsController(mock.Object);

            var newId = 33;
            conrtoller.GetOrAdd(newId);

            mock.Verify(f => f.CreateClient(It.Is<int>(i => i == newId)), Times.Once);
        }

        [Test]
        public void WhenTryGetOldClientThenOldClientInstanceShouldReturned()
        {
            var mock = new Mock<IClientFacrory>();

            var conrtoller = new SimpleClientsController(mock.Object);

            var newId = 55;
            var clientOld = conrtoller.GetOrAdd(newId);
            var clientNew = conrtoller.GetOrAdd(newId);

            mock.Verify(f => f.CreateClient(It.Is<int>(i => i == newId)), Times.Once);

            Assert.That(clientNew, Is.EqualTo(clientOld));
        }
    }
}