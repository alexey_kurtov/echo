﻿using System;
using System.Timers;
using EchoApplication.Domain;
using EchoApplication.Domain.Impl;
using EchoApplication.Transport;
using Moq;
using NUnit.Framework;
using EchoApplication.Common.Tests;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class TimedRoomsControllerTests
    {
        [Test]
        public void WhenInitializeWithNullTimerThenShouldArgumentNullException()
        {
           var ex = Assert.Throws<ArgumentNullException>(() => new TimedRoomsController(new Mock<IRoomFacrory>().Object, null, TimeSpan.Zero));
            Assert.That(ex.ParamName, Is.EqualTo("timer"));
        }[Test]
        public void WhenInitializeWithNullRoomFactoryThenShouldArgumentNullException()
        {
           var ex = Assert.Throws<ArgumentNullException>(() => new TimedRoomsController(null, new Mock<ITimer>().Object, TimeSpan.Zero));
            Assert.That(ex.ParamName, Is.EqualTo("roomFactory"));
        }
        
        [Test]
        public void WhenTryGetNewRoomThenNewRoomInstanceShouldReturned()
        {
            var mock = new Mock<IRoomFacrory>();
            var conrtoller = new TimedRoomsController(mock.Object, new Mock<ITimer>().Object, TimeSpan.Zero);

            var newId = "test room";
            var room = conrtoller.GetOrAdd(newId);

            mock.Verify(f => f.CreateRoom(It.Is<string>(s => s == newId)), Times.Once);
        }

        [Test]
        public void WhenTryGetOldRoomThenOldRoomInstanceShouldReturned()
        {
            var mock = new Mock<IRoomFacrory>();
            var conrtoller = new TimedRoomsController(mock.Object, new Mock<ITimer>().Object, TimeSpan.Zero);

            var newId = "old room";
            var roomOld = conrtoller.GetOrAdd(newId);
            var roomNew = conrtoller.GetOrAdd(newId);

            Assert.That(roomNew, Is.EqualTo(roomOld));

            mock.Verify(f => f.CreateRoom(It.Is<string>(s => s == newId)), Times.Once);
        }

        
        [Test]
        public void WhenTimerElapsedThenNotActiveRoomShouldDeleted()
        {
            var now = DateTime.Now;
            var lifeTime = TimeSpan.FromSeconds(1);

            var factoryMock = new Mock<IRoomFacrory>();


            factoryMock.Setup(f => f.CreateRoom(It.Is<string>(s => s == "active"))).Returns(() =>
            {
                var m = new Mock<RoomAbstraction>();
                m.SetupProperty(r => r.LastActivity, now + lifeTime);
                return m.Object;
            });

            factoryMock.Setup(f => f.CreateRoom(It.Is<string>(s => s == "notActive"))).Returns(() =>
            {
                var m = new Mock<RoomAbstraction>();
                m.SetupProperty(r => r.LastActivity, now);
                return m.Object;
            });

            var timerMock = new Mock<ITimer>();
            
            var conrtoller = new TimedRoomsController(factoryMock.Object, timerMock.Object, lifeTime);
            
            var activeRoom = conrtoller.GetOrAdd("active");
            var notActiveRoom = conrtoller.GetOrAdd("notActive");

            
            timerMock.Raise(t => t.Elapsed += null, new EventArgs<DateTime>(DateTime.Now + lifeTime));

           
           Assert.That(activeRoom, Is.EqualTo(conrtoller.GetOrAdd("active")));
           Assert.That(notActiveRoom, Is.Not.EqualTo(conrtoller.GetOrAdd("notActive")));

            factoryMock.Verify(f => f.CreateRoom(It.Is<string>(s => s == "notActive")), Times.Exactly(2));
            factoryMock.Verify(f => f.CreateRoom(It.Is<string>(s => s == "active")), Times.Once);
        }

        [Test]
        public void WhenTimerElapsedThenNotActiveRoomShouldClosed()
        {
            var now = DateTime.Now;
            var lifeTime = TimeSpan.FromSeconds(1);

            var factoryMock = new Mock<IRoomFacrory>();

            var notActiveRoom = new Mock<RoomAbstraction>();
            notActiveRoom.SetupProperty(r => r.LastActivity, now);

            var activeRoom = new Mock<RoomAbstraction>();
            activeRoom.SetupProperty(r => r.LastActivity, now + lifeTime);
            
            factoryMock.Setup(f => f.CreateRoom(It.Is<string>(s => s == "notActive"))).Returns(() => notActiveRoom.Object);
            factoryMock.Setup(f => f.CreateRoom(It.Is<string>(s => s == "active"))).Returns(() => activeRoom.Object);

            var timerMock = new Mock<ITimer>();

            var conrtoller = new TimedRoomsController(factoryMock.Object, timerMock.Object, lifeTime);

            conrtoller.GetOrAdd("active");
            conrtoller.GetOrAdd("notActive");

            timerMock.Raise(t => t.Elapsed += null, new EventArgs<DateTime>(DateTime.Now + lifeTime));


            notActiveRoom.Verify(r => r.Close(), Times.Once);
            activeRoom.Verify(r => r.Close(), Times.Never);
        }


        [Test]
        public void WhenControllerInitializeThenTimerShouldStarted()
        {
            var mock = new Mock<ITimer>();
            var controller = new TimedRoomsController(new Mock<IRoomFacrory>().Object, mock.Object, TimeSpan.Zero);

            mock.Verify(t => t.Start(), Times.Once);
        }

        [Test]
        public void WhenControllerDisposeThenTimerShouldStoped()
        {
            var mock = new Mock<ITimer>();
            var controller = new TimedRoomsController(new Mock<IRoomFacrory>().Object, mock.Object, TimeSpan.Zero);
            controller.Dispose();

            mock.Verify(t => t.Stop(), Times.Once);
        }
    }

    public abstract class RoomAbstraction : IRoom
    {
        public abstract string Id { get; set; }
        public abstract DateTime LastActivity { get; set; }
        public abstract void Connect(IClient client);

        public abstract void Disconnect(IClient client);
        public abstract void Close();
    }
}