﻿using System;
using System.Threading;
using EchoApplication.Domain;
using EchoApplication.Domain.Impl;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class EchoRoomTests
    {
        [Test]
        public void WhenInitializeWithNullIdThenShouldArgumentException()
        {
            var ex = Assert.Catch<ArgumentException>(() => new EchoRoom(null));
            Assert.That(ex.ParamName, Is.EqualTo("id"));
        }

        [Test]
        public void WhenInitializeWithEmptyIdThenShouldArgumentException()
        {
            var ex = Assert.Catch<ArgumentException>(() => new EchoRoom(""));
            Assert.That(ex.ParamName, Is.EqualTo("id"));
        }

        [Test]
        public void WhenConnectToNullClientThenShouldArgumentNullException()
        {
            var ex = Assert.Catch<ArgumentNullException>(() => new EchoRoom("1").Connect(null));
            Assert.That(ex.ParamName, Is.EqualTo("client"));
        }

        [Test]
        public void WhenDisconnectToNullClientThenShouldArgumentNullException()
        {
           var ex = Assert.Catch<ArgumentNullException>(() => new EchoRoom("2").Disconnect(null));
            Assert.That(ex.ParamName, Is.EqualTo("client"));
        }

        [Test]
        public void WhenConnectToClientThenShouldCallOnceClientSubscribeMethod()
        {
            var room = new EchoRoom("1");
            var mock = new Mock<IClient>();
            
            room.Connect(mock.Object);

            mock.Verify(c => c.Subscribe(It.IsAny<Action<string>>()), Times.Once);
        }


        [Test]
        public void WhenDisconnectToClientThenShouldCallOnceClientUnsubscribeMethod()
        {
            var room = new EchoRoom("1");
            var mock = new Mock<IClient>();

            room.Disconnect(mock.Object);

            mock.Verify(c => c.Unsubscribe(), Times.Once);
        }

        [Test]
        public void WhenClientPublishMessageThenAllClientShouldNotified()
        {
            var room = new EchoRoom("1");
            
            var client1 = new Mock<IClient>();
            Action<string> subscribeAction = null;
            client1.Setup(c => c.Subscribe(It.IsAny<Action<string>>())).Callback((Action<string> action) => subscribeAction = action);

            var client2 = new Mock<IClient>();
            var client3 = new Mock<IClient>();
            
            room.Connect(client1.Object);
            room.Connect(client2.Object);
            room.Connect(client3.Object);


            var message = "message";
            subscribeAction(message);

            client1.Verify(c => c.Notify(It.Is<string>(m => m == message)), Times.Once);
            client2.Verify(c => c.Notify(It.Is<string>(m => m == message)), Times.Once);
            client3.Verify(c => c.Notify(It.Is<string>(m => m == message)), Times.Once);
        }

        [Test]
        public void WhenClientPublishMessageThenUnsubscribedClientShouldNotNotified()
        {
            var room = new EchoRoom("1");

            var client1 = new Mock<IClient>();
            Action<string> subscribeAction = null;
            client1.Setup(c => c.Subscribe(It.IsAny<Action<string>>())).Callback(( Action<string> action) => subscribeAction = action);

            var client2 = new Mock<IClient>();

            room.Connect(client1.Object);
            room.Connect(client2.Object);

            room.Disconnect(client2.Object);


            var message = "message";
            subscribeAction(message);

            client1.Verify(c => c.Notify(It.Is<string>(m => m == message)), Times.Once);
            client2.Verify(c => c.Notify(It.Is<string>(m => m == message)), Times.Never);
        }

        [Test]
        public void WhenClientPublishMessageThenLastActivityShouldChanged()
        {
            var syncEvent = new AutoResetEvent(false);
            var room = new EchoRoom("1");

            var client = new Mock<IClient>();
            Action<string> subscribeAction = null;
            client.Setup(c => c.Subscribe(It.IsAny<Action<string>>())).Callback((Action<string> action) =>
                                                                                    {
                                                                                        subscribeAction = action;
                                                                                        syncEvent.Set();
                                                                                    });
            room.Connect(client.Object);

            var time1 = room.LastActivity;
            Thread.Sleep(100);

            syncEvent.WaitOne(TimeSpan.FromSeconds(2));
            subscribeAction("message");
            
            var time2 = room.LastActivity;

            Assert.That(time1, Is.Not.EqualTo(time2));
        }

        [Test]
        public void WhenClientConnectedToRoomThenLastActivityShouldChanged()
        {
            var room = new EchoRoom("1");
            var client = new Mock<IClient>();

            var time1 = room.LastActivity;

            room.Connect(client.Object);

            var time2 = room.LastActivity;

            Assert.That(time1, Is.Not.EqualTo(time2));
        }

        [Test]
        public void WhenRoomClossedThenAllClientShouldDisconnected()
        {
            var room = new EchoRoom("1");
            var client1 = new Mock<IClient>();
            var client2 = new Mock<IClient>();
            var client3 = new Mock<IClient>();
            
            room.Connect(client1.Object);
            room.Connect(client2.Object);
            room.Connect(client3.Object);

            room.Close();

            client1.Verify(c => c.Unsubscribe(), Times.Once);
            client2.Verify(c => c.Unsubscribe(), Times.Once);
            client3.Verify(c => c.Unsubscribe(), Times.Once);
        }
    }
}
