﻿using System;
using EchoApplication.Domain;
using EchoApplication.Domain.Impl;
using EchoApplication.Transport;
using EchoApplication.Transport.DTO;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class EchoClientTests
    {

        [Test]
        public void WhenCreateWithNullMessageHandlerThenShouldThrowArgumentNullException()
        {
            var ex = Assert.Catch<ArgumentNullException>(() => new EchoClient(11, null));
            Assert.That(ex.ParamName, Is.EqualTo("messageHandler"));
        }

        [Test]
        public void WhenClientNotifyMessageThenShouldSendOnceMessageToTransportLayer()
        {
            var mock = new Mock<IServerMessageHandler>();
            var clientId = 54;
            var client = new EchoClient(clientId, mock.Object);
            string message = "test message";

            client.Notify(message);
            mock.Verify(c => c.Send(It.Is<StringMessage>(m => m.Message == message && m.ClientId == clientId)), Times.Once);
        }

        [Test]
        public void WhenSubscribeWithNullActionThenShouldThrowArgumentNullException()
        {
            var mock = new Mock<IServerMessageHandler>();
            var client = new EchoClient(1, mock.Object);

            Assert.Catch<ArgumentNullException>(() => client.Subscribe(null));
        }

        [Test]
        public void WhenClientSubscribeThenShouldSubscribeToTransportLayer()
        {
            var mock = new Mock<IServerMessageHandler>();

            var clientId = 43;
            var client = new EchoClient(clientId, mock.Object);
            Action<string> processAction = s => {};

            client.Subscribe(processAction);
            
            mock.Verify(c => c.SubscribeBy(It.Is<int>(id => id == clientId), It.IsNotNull<Action<StringMessage>>()), Times.Once);
        }

        [Test]
        public void WhenClientUnsubscribeThenShouldSubscriberCallUsubscribeMethod()
        {
            var messageHandlerMock = new Mock<IServerMessageHandler>();
            var subscriberMock = new Mock<ISubscriber>();

            messageHandlerMock.Setup(c => c.SubscribeBy(It.IsAny<int>(), It.IsAny<Action<StringMessage>>())).Returns(subscriberMock.Object);

            var clientId = 43;
            var client = new EchoClient(clientId, messageHandlerMock.Object);
            Action<string> processAction = s => { };

            client.Subscribe(processAction);
            client.Unsubscribe();

            subscriberMock.Verify(c => c.Unsubscribe(), Times.Once);
        }



        [Test]
        public void WhenJoinToRoomThenRoomShouldConnectedToThisClient()
        {
            var messageHandlerMock = new Mock<IServerMessageHandler>();
            var roomMock = new Mock<IRoom>();

            var client = new EchoClient(11, messageHandlerMock.Object);

            client.JoinToRoom(roomMock.Object);

           
            roomMock.Verify(c => c.Connect(client), Times.Once);
        }

        [Test]
        public void WhenJoinToNullRoomThenShouldThrowArgumentException()
        {
            var messageHandlerMock = new Mock<IServerMessageHandler>();
            
            var client = new EchoClient(11, messageHandlerMock.Object);

            Assert.Catch<ArgumentNullException>(() => client.JoinToRoom(null));
        }

        [Test]
        public void WhenJoinToNewRoomThenOldRoomShouldDisconected()
        {
            var messageHandlerMock = new Mock<IServerMessageHandler>();
            var room1Mock = new Mock<IRoom>();
            var room2Mock = new Mock<IRoom>();

            var client = new EchoClient(11, messageHandlerMock.Object);

            client.JoinToRoom(room1Mock.Object);
            room1Mock.Verify(c => c.Disconnect(client), Times.Never);

            client.JoinToRoom(room2Mock.Object);
            room1Mock.Verify(c => c.Disconnect(client), Times.Once);
        }
    }
}