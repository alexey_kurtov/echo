﻿using System;
using EchoApplication.Domain.Impl;
using EchoApplication.Transport;
using Moq;
using NUnit.Framework;

namespace EchoApplication.Tests
{
    [TestFixture]
    public class EchoEntityFactoryTests
    {
        [Test]
        public void WhenInitializeWithNullMessageHandlerThenThrowArgumentNullException()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => new EchoEntityFactory(null));
            Assert.That(ex.ParamName, Is.EqualTo("messageHandler"));
        }

        [Test]
        public void WhenCreateRoomThenNewRoomInstanceWasInitialized()
        {
            var factory = new EchoEntityFactory(new Mock<IServerMessageHandler>().Object);

            var room = factory.CreateRoom("newRoom");
            Assert.That(room.Id, Is.EqualTo("newRoom"));
        }


        [Test]
        public void WhenCreateClientThenNewClientInstanceWasInitialized()
        {
            var factory = new EchoEntityFactory(new Mock<IServerMessageHandler>().Object);

            var client = factory.CreateClient(54);
            Assert.That(client.Id, Is.EqualTo(54));
        }
    }
}