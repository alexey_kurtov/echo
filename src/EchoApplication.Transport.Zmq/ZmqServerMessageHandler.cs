﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EchoApplication.Transport.DTO;
using EchoApplication.Transport.Zmq.Subscribers;
using ZeroMQ;

namespace EchoApplication.Transport.Zmq
{
    public class ZmqServerMessageHandler : IDisposable, IServerMessageHandler
    {
        private readonly ISerializer _serializer;
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly ZSocket _router;
        private readonly ConcurrentDictionary<int, byte[]> _clientMapping;
        private readonly List<ZmqSubscriber> _subscribers;
        private readonly object _sync = new object();

        public ZmqServerMessageHandler(int bindPort, ISerializer serializer)
        {
            _serializer = serializer;
            _cancellationTokenSource = new CancellationTokenSource();

            _clientMapping = new ConcurrentDictionary<int, byte[]>();
            _subscribers = new List<ZmqSubscriber>();

            _router = new ZSocket(ZSocketType.ROUTER);
            _router.Bind($"tcp://*:{bindPort}");

            Task.Run(() => processMessages(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
        }

        public void Send(ICommunicationMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            byte[] networkId;
            if(_clientMapping.TryGetValue(message.ClientId, out networkId))
            {
                send(networkId, message);
            }

        }


        private void send(byte[] networkId, ICommunicationMessage message)
        {

            var frame = new ZFrame(_serializer.Serialize(new TransportMsg
            {
                ClientId = message.ClientId,
                Type = message.GetType().Name,
                Data = _serializer.Serialize(message)
            }));


            lock (_sync)
            {
                _router.SendMore(new ZFrame(networkId));
                _router.SendFrame(frame);
            }
        }

        public ISubscriber SubscribeBy<T>(int clientId, Action<T> handler) where T : ICommunicationMessage
        {

            var subscriber = new CheckClientAndTypeSubscription<T>(clientId, handler, _serializer);
             _subscribers.Add(subscriber);
           
            return subscriber;
        }

        public ISubscriber SubscribeAll<T>(Action<T> handler) where T : ICommunicationMessage
        {
            var subscriber = new CheckTypeSubscriber<T>(handler,_serializer);
            _subscribers.Add(subscriber);
            
            return subscriber;
        }

        private void processMessages(CancellationToken token)
        {
            
            while (!token.IsCancellationRequested)
            {
                int frameToReceive = 2;
                List<ZFrame> frames = new List<ZFrame>();
                bool received;
                lock (_sync)
                {
                    ZError err;
                    received = _router.ReceiveFrames(ref frameToReceive, ref frames, ZSocketFlags.DontWait, out err);
                }

                if (received)
                {
                    var networkId = frames[0].Read();
                    var msg = _serializer.Deserialize<TransportMsg>(frames[1]);

                    _clientMapping.AddOrUpdate(msg.ClientId, id => networkId, (id, oldNetworkId) =>
                    {
                        if (!oldNetworkId.SequenceEqual(networkId))
                        {
                            lock (_sync)
                            {
                                send(oldNetworkId, new CloseMessage {ClientId = id});
                            }
                        }

                        return networkId;
                    });

                    var needClean = false;
                    foreach (var subscriber in _subscribers.ToArray())
                    {
                        if (subscriber.Unsubscribed)
                        {
                            needClean = true;
                        }
                        else
                        {
                            subscriber.Notify(msg.ClientId, msg);
                        }

                    }
                    if (needClean)
                    {
                        _subscribers.RemoveAll(subscriber => subscriber.Unsubscribed);

                    }


                    foreach (var frame in frames)
                    {
                        frame.Dispose();
                    }
                }
                else
                {
                    Thread.Sleep(20);
                }
            }
        }


        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _clientMapping.Clear();

            _router.Close();
            _router.Dispose();
        }
    }
}
