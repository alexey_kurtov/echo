﻿namespace EchoApplication.Transport.Zmq.Subscribers
{
    public abstract class ZmqSubscriber : ISubscriber
    {
        public bool Unsubscribed { get; private set; }

        public void Notify(int clientId, TransportMsg message)
        {
            if (!Unsubscribed && IsValid(clientId, message))
            {
                ApplyNotification(clientId, message);
            }
        }

        protected abstract void ApplyNotification(int clientId, TransportMsg message);


        protected abstract bool IsValid(int clientId, TransportMsg message);

        public virtual void Unsubscribe()
        {
            Unsubscribed = true;
        }
    }
}