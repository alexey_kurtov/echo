﻿using System;
using EchoApplication.Transport.DTO;
using EchoApplication.Common;

namespace EchoApplication.Transport.Zmq.Subscribers
{
    public class CheckTypeSubscriber<T> : ZmqSubscriber where T : ICommunicationMessage
    {
        private readonly ISerializer _serializer;
        private readonly Action<T> _action;
        private readonly string _typeName;

        public CheckTypeSubscriber(Action<T> action, ISerializer serializer)
        {
            _serializer = serializer.ThrowIfNull("serializer");
            _typeName = typeof(T).Name;
            _action = action.ThrowIfNull("action");
        }

        protected override void ApplyNotification(int clientId, TransportMsg message)
        {
            _action(_serializer.Deserialize<T>(message.Data));
        }

        protected override bool IsValid(int clientId, TransportMsg message)
        {
            return _typeName == message.Type;
        }
    }
}