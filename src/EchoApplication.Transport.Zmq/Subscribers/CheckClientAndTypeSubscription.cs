using System;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport.Zmq.Subscribers
{
    public class CheckClientAndTypeSubscription<T> : CheckTypeSubscriber<T> where T : ICommunicationMessage
    {
        private readonly int _clientId;
        public CheckClientAndTypeSubscription(int clientId, Action<T> action, ISerializer serializer) : base(action, serializer)
        {
            _clientId = clientId;
        }
        

        protected override bool IsValid(int clientId, TransportMsg message)
        {
            return _clientId == clientId && base.IsValid(clientId, message);
        }
    }
}