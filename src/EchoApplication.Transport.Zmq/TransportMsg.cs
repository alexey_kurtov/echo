﻿using System.Runtime.Serialization;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport.Zmq
{
    [DataContract]
    public class TransportMsg : ICommunicationMessage
    {
        [DataMember(Order = 1)]
        public int ClientId { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; }

        [DataMember(Order = 3)]
        public byte[] Data { get; set; }
    }
}