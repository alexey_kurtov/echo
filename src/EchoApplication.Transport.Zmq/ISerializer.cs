﻿using System.IO;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport.Zmq
{
    /// <summary>
    /// Represent serializer for communication objects
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Serialize communication object to byte array
        /// </summary>
        /// <typeparam name="T">Type of communication object</typeparam>
        /// <param name="message">Communication object instance</param>
        byte[] Serialize<T>(T message) where T : ICommunicationMessage;


        /// <summary>
        /// Deserialize byte array to communication object
        /// </summary>
        /// <typeparam name="T">Type of communication object</typeparam>
        /// <param name="stream">Raw byte stream</param>
        T Deserialize<T>(Stream stream) where T : ICommunicationMessage;

        /// <summary>
        /// Deserialize byte array to communication object
        /// </summary>
        /// <typeparam name="T">Type of communication object</typeparam>
        /// <param name="data">Raw byte array</param>
        T Deserialize<T>(byte[] data) where T : ICommunicationMessage;
    }
}