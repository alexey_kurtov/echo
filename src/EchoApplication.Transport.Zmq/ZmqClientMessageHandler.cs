using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EchoApplication.Transport.DTO;
using EchoApplication.Transport.Zmq.Subscribers;
using ZeroMQ;

namespace EchoApplication.Transport.Zmq
{
    public class  ZmqClientMessageHandler : IDisposable, IClientMessageHandler
    {
        private readonly ZSocket _dialer;
        private readonly ISerializer _serializer;
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly List<ZmqSubscriber> _subscribers;
        private readonly object _sync = new object();

        public ZmqClientMessageHandler(string address, ISerializer serializer)
        {
            _serializer = serializer;
            _cancellationTokenSource = new CancellationTokenSource();

            _subscribers = new List<ZmqSubscriber>();

            _dialer = new ZSocket(ZSocketType.DEALER);
            _dialer.Connect(address);

            Task.Run(() => processMessages(_cancellationTokenSource.Token),_cancellationTokenSource.Token);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();

            _dialer.Close();
            _dialer.Dispose();
        }

        public ISubscriber Subscribe<T>(Action<T> handler) where T : ICommunicationMessage
        {
            var subscriber = new CheckTypeSubscriber<T>(handler, _serializer);

            _subscribers.Add(subscriber);
            
            return subscriber;
        }

        public void Send(ICommunicationMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            var frame = new ZFrame(_serializer.Serialize(new TransportMsg
            {
                ClientId = message.ClientId,
                Type = message.GetType().Name,
                Data = _serializer.Serialize(message)
            }));

            lock (_sync)
            {
                _dialer.SendFrame(frame, ZSocketFlags.DontWait);
            }
        }

        private void processMessages(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                ZFrame frame = null;
                try
                {
                    lock (_sync)
                    {
                        ZError error;
                        frame = _dialer.ReceiveFrame(ZSocketFlags.DontWait, out error);
                    }

                    if (frame == null)
                    {
                        Thread.Sleep(20);
                        continue;
                    }

                    var msg = _serializer.Deserialize<TransportMsg>(frame);

                    var needClean = false;
                    foreach (var subscriber in _subscribers.ToArray())
                    {
                        if (subscriber.Unsubscribed)
                        {
                            needClean = true;
                        }
                        else
                        {
                            subscriber.Notify(msg.ClientId, msg);
                        }

                    }
                    if (needClean)
                    {
                        _subscribers.RemoveAll(subscriber => subscriber.Unsubscribed);

                    }
                }
                finally
                {
                    frame?.Dispose();
                }

               
            }
        }

    }
}