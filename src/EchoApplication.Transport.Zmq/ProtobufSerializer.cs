﻿using System.IO;
using System.Runtime.Serialization;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport.Zmq
{
    public class ProtobufSerializer : ISerializer
    {
        public byte[] Serialize<T>(T message) where T : ICommunicationMessage
        {
            if (message == null)
            {
                throw new SerializationException("Message for serialization is null");
            }

            using (var stream = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize(stream, message);
                return stream.ToArray();
            }
        }

        public T Deserialize<T>(Stream stream) where T: ICommunicationMessage
        {
            if (stream == null)
            {
                throw new SerializationException("Stream for deserialization is null");
            }

            return ProtoBuf.Serializer.Deserialize<T>(stream);
        }

        public T Deserialize<T>(byte[] data) where T : ICommunicationMessage
        {
            if (data == null)
            {
                throw new SerializationException("Byte array for deserialization is null");
            }

            using (var stream = new MemoryStream(data))
            {
                return Deserialize<T>(stream);
            }
        }
    }
}