﻿using System.Runtime.Serialization;

namespace EchoApplication.Transport.DTO
{
    /// <summary>
    /// Represent string message
    /// </summary>
    [DataContract]
    public class StringMessage : ICommunicationMessage
    {
        /// <summary>
        /// Client identifier
        /// </summary>
        [DataMember(Order = 1)]
        public int ClientId { get; set; }

        /// <summary>
        /// Message value
        /// </summary>
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }
}