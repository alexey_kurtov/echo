﻿using System.Runtime.Serialization;

namespace EchoApplication.Transport.DTO
{
    /// <summary>
    /// Represent message for join to echo room
    /// </summary>
    [DataContract]
    public class JoinMessage : ICommunicationMessage
    {
        /// <summary>
        /// Client identifier
        /// </summary>
        [DataMember(Order = 1)]
        public int ClientId { get; set; }

        /// <summary>
        /// Room identifier
        /// </summary>
        [DataMember(Order = 2)]
        public string RoomId { get; set; }
    }
}