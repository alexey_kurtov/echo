﻿using System.Runtime.Serialization;

namespace EchoApplication.Transport.DTO
{
    /// <summary>
    /// Represent message for notify client for close
    /// </summary>
    [DataContract]
    public class CloseMessage : ICommunicationMessage
    {
        /// <summary>
        /// Client identifier
        /// </summary>
        [DataMember(Order = 1)]
        public int ClientId { get; set; }
    }
}