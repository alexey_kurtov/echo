﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoApplication.Transport.DTO
{
    /// <summary>
    /// Represent interface for communication message
    /// </summary>
    public interface ICommunicationMessage
    {
        int ClientId { get; set; }
    }
}
