using System;

namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent controler of clients
    /// </summary>
    public interface IClientsController
    {
        /// <summary>
        /// Get if exist or add new client by id
        /// </summary>
        /// <param name="id">Client identifier</param>
        /// <returns></returns>
        IClient GetOrAdd(int id);
    }
}