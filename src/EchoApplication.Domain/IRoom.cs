﻿using System;

namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent echo room for clients
    /// </summary>
    public interface IRoom
    {
        /// <summary>
        /// Room identifier
        /// </summary>
        string Id { get; }
        /// <summary>
        /// Last date of clients activity on room
        /// </summary>
        DateTime LastActivity { get; }
        
        /// <summary>
        /// Connect client to room
        /// </summary>
        /// <param name="client">Client instance</param>
        void Connect(IClient client);

        /// <summary>
        /// Disconnect client to room
        /// </summary>
        /// <param name="client">Client instance</param>
        void Disconnect(IClient client);

        /// <summary>
        /// Disconnect all client to room
        /// </summary>
        void Close();

    }
}