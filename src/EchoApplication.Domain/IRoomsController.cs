namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent controler of echo rooms
    /// </summary>
    public interface IRoomsController
    {
        /// <summary>
        /// Get if exist or add new room by id
        /// </summary>
        /// <param name="id">Room identifier</param>
        IRoom GetOrAdd(string id);
    }
}