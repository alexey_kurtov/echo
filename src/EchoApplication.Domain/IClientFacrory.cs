namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent factory for initializing client instance 
    /// </summary>
    public interface IClientFacrory
    {
        /// <summary>
        /// Initialize new client 
        /// </summary>
        /// <param name="id">Client identifier</param>
        IClient CreateClient(int id);
    }
}