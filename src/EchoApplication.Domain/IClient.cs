﻿using System;

namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent client entity
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Client identifier
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Join client to room
        /// </summary>
        /// <param name="room">Room instance</param>
        void JoinToRoom(IRoom room);

        /// <summary>
        /// Sent notification message to client
        /// </summary>
        /// <param name="message">Notification message</param>
        void Notify(string message);

        /// <summary>
        /// Subscribe to receive client messages
        /// </summary>
        /// <param name="processAction">Action to process client message</param>
        void Subscribe(Action<string> processAction);

        /// <summary>
        /// Unsubscribe to receive client messages
        /// </summary>
        void Unsubscribe();
    }
}