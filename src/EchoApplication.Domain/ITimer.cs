using EchoApplication.Common.Tests;
using System;
using System.Timers;

namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent timer 
    /// </summary>
    public interface ITimer
    {
        /// <summary>
        /// Start timer 
        /// </summary>
        void Start();

        /// <summary>
        /// Stop timer 
        /// </summary>
        void Stop();

        /// <summary>Timer event</summary>
        event EventHandler<EventArgs<DateTime>> Elapsed;
    }
}