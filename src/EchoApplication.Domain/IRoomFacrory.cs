﻿namespace EchoApplication.Domain
{
    /// <summary>
    /// Represent factory for initializing room instance
    /// </summary>
    public interface IRoomFacrory
    {
        /// <summary>
        /// Initialize new room
        /// </summary>
        /// <param name="id">Room identifier</param>
        IRoom CreateRoom(string id);
    }
}