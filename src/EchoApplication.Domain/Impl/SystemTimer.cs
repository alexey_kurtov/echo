﻿using System;
using System.Timers;
using EchoApplication.Common.Tests;

namespace EchoApplication.Domain.Impl
{
    public class SystemTimer : ITimer
    {
        private readonly Timer _timer;

        public SystemTimer(TimeSpan interval)
        {
            _timer = new Timer(interval.TotalMilliseconds);
            
        }

        private void raiseEvent(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            var handler = Elapsed;
            handler?.Invoke(this, new EventArgs<DateTime>(elapsedEventArgs.SignalTime));
        }

        public void Start()
        {
            _timer.Start();
            _timer.Elapsed += raiseEvent;
        }

        public void Stop()
        {
            _timer.Elapsed -= raiseEvent;
            _timer.Stop();
        }

        public event EventHandler<EventArgs<DateTime>> Elapsed;
    }
}