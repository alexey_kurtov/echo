﻿using System;
using EchoApplication.Common;
using EchoApplication.Transport;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Domain.Impl
{
    public class EchoClient : IClient
    {
        private readonly IServerMessageHandler _messageHandler;
        private ISubscriber _subscriber;
        private IRoom _room;

        public EchoClient(int id, IServerMessageHandler messageHandler)
        {
            _messageHandler = messageHandler.ThrowIfNull("messageHandler");
            Id = id;
        }

        public int Id { get; }

        public void JoinToRoom(IRoom room)
        {
            room.ThrowIfNull("room");

            _room?.Disconnect(this);

            room.Connect(this);
            _room = room;
        }

        public void Notify(string message)
        {
            _messageHandler.Send(new StringMessage {Message = message, ClientId = Id});
        }

        public void Subscribe(Action<string> processAction)
        {
            processAction.ThrowIfNull("processAction");
            _subscriber = _messageHandler.SubscribeBy<StringMessage>(Id, m => processAction(m.Message));
        }

        public void Unsubscribe()
        {
            if (_subscriber != null)
            {
                _subscriber.Unsubscribe();
                _subscriber = null;
            }
        }
    }
}