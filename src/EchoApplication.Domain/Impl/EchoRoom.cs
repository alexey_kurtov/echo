﻿using System;
using System.Collections.Generic;
using EchoApplication.Common;

namespace EchoApplication.Domain.Impl
{
    public class EchoRoom : IRoom
    {
        private readonly List<IClient> _clients;

        public EchoRoom(string id)
        {
            Id = id.ThrowIfNullOrEmpty("id");
            LastActivity = DateTime.Now;
            _clients = new List<IClient>();
        }

        public string Id { get; }
        public DateTime LastActivity { get; private set; }

        public void Connect(IClient client)
        {
            client.ThrowIfNull("client");
            client.Subscribe(messageProcessing);
            _clients.Add(client);
            LastActivity = DateTime.Now;
        }

        public void Disconnect(IClient client)
        {
            client.ThrowIfNull("client");
            client.Unsubscribe();
            _clients.Remove(client);
        }

        public void Close()
        {
            _clients.ForEach(client => client.Unsubscribe());
            _clients.Clear();
        }

        private void messageProcessing(string message)
        {
            _clients.ForEach(c => c.Notify(message));
            LastActivity = DateTime.Now;
        }
    }
}