﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Timers;
using EchoApplication.Common;
using EchoApplication.Common.Tests;

namespace EchoApplication.Domain.Impl
{
    public class TimedRoomsController : IRoomsController, IDisposable
    {
        private readonly IRoomFacrory _roomFacrory;
        private readonly TimeSpan _lifeTime;
        private readonly ITimer _timer;
        private readonly ConcurrentDictionary<string, IRoom> _rooms;

        public TimedRoomsController(IRoomFacrory roomFacrory, ITimer timer, TimeSpan lifeTime)
        {
            _roomFacrory = roomFacrory.ThrowIfNull("roomFactory");
            _lifeTime = lifeTime;
            _timer = timer.ThrowIfNull("timer");
            _timer.Elapsed += timerOnElapsed;
            _rooms = new ConcurrentDictionary<string, IRoom>();
            _timer.Start();
        }

        public IRoom GetOrAdd(string id)
        {
            return _rooms.GetOrAdd(id, i => _roomFacrory.CreateRoom(id));
        }

        protected virtual bool TryDelete(string id)
        {
            IRoom room;
            var result = _rooms.TryRemove(id, out room);

            if (result)
            {
                room.Close();
            }

            return result;
        }

        public void Dispose()
        {
            _timer.Elapsed -= timerOnElapsed;
            _timer.Stop();
        }

        private void timerOnElapsed(object sender, EventArgs<DateTime> time)
        {
            foreach (var room in _rooms.Where(r => r.Value.LastActivity + _lifeTime <= time.Value))
            {
                TryDelete(room.Key);
            }
        }
    }
}