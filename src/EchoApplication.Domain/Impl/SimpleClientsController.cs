﻿using System.Collections.Concurrent;
using EchoApplication.Common;

namespace EchoApplication.Domain.Impl
{
    public class SimpleClientsController : IClientsController
    {
        private readonly IClientFacrory _clientFacrory;
        private readonly ConcurrentDictionary<int, IClient> _clients;

        public SimpleClientsController(IClientFacrory clientFacrory)
        {
            _clientFacrory = clientFacrory.ThrowIfNull("clientFacrory");
            _clients = new ConcurrentDictionary<int, IClient>();
        }

        public IClient GetOrAdd(int id)
        {
            return _clients.GetOrAdd(id, i => _clientFacrory.CreateClient(id));
        }
    }
}