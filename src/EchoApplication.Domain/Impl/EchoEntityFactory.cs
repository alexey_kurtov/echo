﻿using EchoApplication.Common;
using EchoApplication.Transport;

namespace EchoApplication.Domain.Impl
{
    public class EchoEntityFactory : IClientFacrory, IRoomFacrory
    {
        private readonly IServerMessageHandler _messageHandler;

        public EchoEntityFactory(IServerMessageHandler messageHandler)
        {
            _messageHandler = messageHandler.ThrowIfNull("messageHandler");
        }

        public IClient CreateClient(int id)
        {
            return new EchoClient(id, _messageHandler);
        }

        public IRoom CreateRoom(string id)
        {
            return new EchoRoom(id);
        }
    }
}