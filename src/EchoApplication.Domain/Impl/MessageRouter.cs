﻿using System;
using EchoApplication.Common;
using EchoApplication.Transport;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Domain.Impl
{
    public class MessageRouter : IDisposable
    {
        private readonly IRoomsController _roomsController;
        private readonly IClientsController _clientsController;
        private readonly ISubscriber _joinRoomSubscriber;


        public MessageRouter(IServerMessageHandler messageHandler, IRoomsController roomsController, IClientsController clientsController)
        {
            _roomsController = roomsController.ThrowIfNull("roomsController");
            _clientsController = clientsController.ThrowIfNull("clientsController");
            _joinRoomSubscriber = messageHandler.ThrowIfNull("messageHandler").SubscribeAll<JoinMessage>(m => joinToRoom(m.ClientId, m.RoomId));
        }

        private void joinToRoom(int clientId, string roomId)
        {
            var client = _clientsController.GetOrAdd(clientId);
            var room = _roomsController.GetOrAdd(roomId);

            client.JoinToRoom(room);
        }

        public void Dispose()
        {
            _joinRoomSubscriber.Unsubscribe();
        }
    }
}