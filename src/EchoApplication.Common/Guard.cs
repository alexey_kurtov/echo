﻿using System;

namespace EchoApplication.Common
{
    public static class Guard
    {
        public static T ThrowIfNull<T>(this T source, string paramName)
        {
            if (source == null)
            {
                throw new ArgumentNullException(paramName);
            }

            return source;
        }

        public static string ThrowIfNullOrEmpty(this string source, string paramName)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("Parameter is null or empty", paramName);
            }

            return source;
        }
    }
}
