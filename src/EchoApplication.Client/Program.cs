﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EchoApplication.Transport.Zmq;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Client
{
    class Program
    {
        private static AutoResetEvent _keepRunning = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            var address = ConfigurationManager.AppSettings["serverAddress"];

            int clientId = 0;

            while (clientId <= 0)
            {
                Console.Write("Input client id (number greater than zero): ");
                var val = Console.ReadLine();
                if (!int.TryParse(val, out clientId) || clientId <= 0)
                {
                    Console.WriteLine("Incorrect...");
                }
            }
            
            string roomId = null;

            while (string.IsNullOrEmpty(roomId))
            {
                Console.Write("Input room id (string): ");
                roomId = Console.ReadLine();
                if (string.IsNullOrEmpty(roomId))
                {
                    Console.WriteLine("Incorrect...");
                }
            }

            Console.WriteLine();
            Console.WriteLine("Press Ctrl + C to Exit...");
            Console.WriteLine();

            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e)
            {
                _keepRunning.Set();
                e.Cancel = true;
            };

            
            var messageHandler = new ZmqClientMessageHandler(address, new ProtobufSerializer());

            messageHandler.Send(new JoinMessage() {ClientId = clientId, RoomId = roomId});
            Console.WriteLine($"Connect to '{address}'");
            Console.WriteLine($"Join to room '{roomId}'");

            messageHandler.Subscribe<StringMessage>(m => Console.WriteLine($"[{m.ClientId}] {m.Message}"));
            messageHandler.Subscribe<CloseMessage>(m => _keepRunning.Set());

            var exit = false;
            Timer timer = new Timer(state =>
            {
                if (!exit)
                {
                    messageHandler.Send(new StringMessage { ClientId = clientId, Message = $"ping from [{clientId}] client" });
                }
            }, null, TimeSpan.FromMilliseconds(100), TimeSpan.FromMilliseconds(100));

            _keepRunning.WaitOne();
            exit = true;
            timer.Dispose();
            messageHandler.Dispose();
        }

    }
}
