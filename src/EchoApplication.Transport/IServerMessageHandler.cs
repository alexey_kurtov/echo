﻿using System;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport
{
    /// <summary>
    /// Represent message handler for server messages
    /// </summary>
    public interface IServerMessageHandler
    {
        /// <summary>
        /// Send string message to server
        /// </summary>
        /// <param name="message">Message to send</param>
        void Send(ICommunicationMessage message);

        /// <summary>
        /// Subscribe to receive messages by client
        /// </summary>
        /// <param name="clientId">Client identifier</param>
        /// <param name="handler">Action to process message</param>
        ISubscriber SubscribeBy<T>(int clientId, Action<T> handler) where T : ICommunicationMessage;

        /// <summary>
        /// Subscribe to receive all messages by type
        /// </summary>
        /// <param name="handler">Action to process message</param>
        ISubscriber SubscribeAll<T>(Action<T> handler) where T : ICommunicationMessage;

    }
}