﻿using System;
using EchoApplication.Transport.DTO;

namespace EchoApplication.Transport
{
    /// <summary>
    /// Represent message handler for client messages
    /// </summary>
    public interface IClientMessageHandler
    {
        /// <summary>
        /// Subscribe to receive client messages
        /// </summary>
        /// <param name="handler">Action to process client message</param>
        ISubscriber Subscribe<T>(Action<T> handler) where T : ICommunicationMessage;

        /// <summary>
        /// Send string message to specific client
        /// </summary>
        /// <param name="message">Message to send</param>
        void Send(ICommunicationMessage message);
    }
}