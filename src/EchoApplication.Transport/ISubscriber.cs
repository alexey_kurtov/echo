﻿namespace EchoApplication.Transport
{
    public interface ISubscriber
    {
        void Unsubscribe();
    }
}