﻿using System;
using System.Configuration;
using NLog;
using Topshelf;

namespace EchoApplication.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            AppDomain.CurrentDomain.UnhandledException += currentDomainUnhandledException;

            var bindPortStr = ConfigurationManager.AppSettings["bindPort"];
            var roomLifeTimeStr = ConfigurationManager.AppSettings["roomLifeTime"];
            var checkNotActiveRoomIntervalStr = ConfigurationManager.AppSettings["activeRoomCheckInterval"];

            int bindPort;
            if (!int.TryParse(bindPortStr, out bindPort))
            {
                bindPort = 3333;
            }

            TimeSpan roomLifeTime;
            if (!TimeSpan.TryParse(roomLifeTimeStr, out roomLifeTime))
            {
                roomLifeTime = TimeSpan.FromMinutes(1);
            }


            TimeSpan checkNotActiveRoomInterval;
            if (!TimeSpan.TryParse(checkNotActiveRoomIntervalStr, out checkNotActiveRoomInterval))
            {
                checkNotActiveRoomInterval = TimeSpan.FromSeconds(1);
            }

            HostFactory.Run(x =>
            {
                x.UseNLog();

                x.Service<Application>(s =>
                {
                    s.ConstructUsing(name => new Application(bindPort, roomLifeTime, checkNotActiveRoomInterval));

                    s.WhenStarted((app, control) =>
                    {
                        app.Start();
                        return true;
                    });

                    s.WhenStopped((app, control) =>
                    {
                        app.Stop();
                        return true;
                    });
                });


                x.RunAsLocalSystem();

                x.SetDescription("Room echo server");
                x.SetDisplayName("Echo server");
                x.SetServiceName("EchoServer");
            });
        }

        private static void currentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex != null)
            {
                Console.WriteLine(e.ExceptionObject);
                LogManager.GetCurrentClassLogger().Error(ex, "Unhandled exception");
            }
        }
    }
}
