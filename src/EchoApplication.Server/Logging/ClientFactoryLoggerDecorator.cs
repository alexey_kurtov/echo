﻿using EchoApplication.Domain;
using NLog;

namespace EchoApplication.Server.Logging
{
    public class ClientFactoryLoggerDecorator : IClientFacrory
    {
        private readonly ILogger _logger;
        private readonly IClientFacrory _clientFacrory;

        public ClientFactoryLoggerDecorator(IClientFacrory clientFacrory)
        {
            var type = clientFacrory.GetType();
            _logger = LogManager.GetLogger(type.Name, type);
            _clientFacrory = clientFacrory;
        }
        
        public IClient CreateClient(int id)
        {
            _logger.Info($"Client '{id}' was initialized");
            return _clientFacrory.CreateClient(id);
        }
    }
}