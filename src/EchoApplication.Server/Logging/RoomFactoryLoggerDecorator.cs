﻿using EchoApplication.Domain;
using NLog;

namespace EchoApplication.Server.Logging
{
    public class RoomFactoryLoggerDecorator : IRoomFacrory
    {
        private readonly ILogger _logger;
        private readonly IRoomFacrory _roomFacrory;

        public RoomFactoryLoggerDecorator(IRoomFacrory roomFacrory)
        {
            var type = roomFacrory.GetType();
            _logger = LogManager.GetLogger(type.Name, type);
            _roomFacrory = roomFacrory;
        }

        public IRoom CreateRoom(string id)
        {
            _logger.Info($"Room '{id}' was created");
            return new RoomLoggerDecorator(_roomFacrory.CreateRoom(id));
        }
    }
}