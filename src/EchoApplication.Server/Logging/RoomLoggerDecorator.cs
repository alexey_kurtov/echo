﻿using System;
using EchoApplication.Domain;
using NLog;

namespace EchoApplication.Server.Logging
{
    public class RoomLoggerDecorator : IRoom
    {
        private readonly IRoom _room;
        private readonly Logger _logger;

        public RoomLoggerDecorator(IRoom room)
        {
            var type = room.GetType();
            _logger = LogManager.GetLogger(type.Name, type);
            _room = room;
        }

        public string Id => _room.Id;
        public DateTime LastActivity => _room.LastActivity;
        public void Connect(IClient client)
        {
            _room.Connect(client);
            _logger.Info($"Client '{client.Id}' was connected to room '{Id}'");
        }

        public void Disconnect(IClient client)
        {
            _room.Disconnect(client);
            _logger.Info($"Client '{client.Id}' was disconnected to room '{Id}'");
        }

        public void Close()
        {
            _room.Close();
            _logger.Info($"Room '{Id}' was deleted");
        }
    }
}