using System;
using EchoApplication.Domain.Impl;
using EchoApplication.Server.Logging;
using EchoApplication.Transport.Zmq;
using NLog;

namespace EchoApplication.Server
{
    internal class Application
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly int _bindPort;
        private readonly TimeSpan _roomLifeTime;
        private readonly TimeSpan _activeRoomCheckInterval;
        private readonly ProtobufSerializer _protobufSerializer;
        private ZmqServerMessageHandler _messageHandler;
        private MessageRouter _router;
        private TimedRoomsController _roomsController;

        public Application(int bindPort, TimeSpan roomLifeTime, TimeSpan activeRoomCheckInterval)
        {

            _bindPort = bindPort;
            _roomLifeTime = roomLifeTime;
            _activeRoomCheckInterval = activeRoomCheckInterval;
            _protobufSerializer = new ProtobufSerializer();
        }

        public void Start()
        {
            _logger.Info($"Bind to 'tcp//*:{_bindPort}'");
            _logger.Info($"Room life time '{_roomLifeTime}'");
            _logger.Info($"Check active room interval '{_activeRoomCheckInterval}'");

            _messageHandler = new ZmqServerMessageHandler(_bindPort, _protobufSerializer);
            var entityFactory = new EchoEntityFactory(_messageHandler);

            var timer = new SystemTimer(_activeRoomCheckInterval);

            _roomsController = new TimedRoomsController(new RoomFactoryLoggerDecorator(entityFactory), timer, _roomLifeTime);
            var clientsController = new SimpleClientsController(new ClientFactoryLoggerDecorator(entityFactory));
            
            _router = new MessageRouter(_messageHandler, _roomsController, clientsController);
        }

        public void Stop()
        {
            _router?.Dispose();
            _roomsController?.Dispose();
            
            _messageHandler?.Dispose();
        }
    }
}